package ru.dolbak.retrofitexamplejava;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherApi {
    @GET("weather")
    public Call<WeatherData> getWeather(@Query("q") String city, @Query("appid") String API_KEY, @Query("units") String units);
}
