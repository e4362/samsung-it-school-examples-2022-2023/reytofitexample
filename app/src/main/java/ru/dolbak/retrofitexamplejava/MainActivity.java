
package ru.dolbak.retrofitexamplejava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    String API_KEY = "67c56251ee16702602cf14ab9f65a802";
    TextView textView;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView2);
        editText = findViewById(R.id.editTextText);
    }


    public void findWeather(View view) {
        NetworkService.getInstance()
                .getWeatherApi()
                .getWeather(editText.getText().toString(), API_KEY, "metric")
                .enqueue(new Callback<WeatherData>() {
                    @Override
                    public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                        WeatherData weatherData = response.body();

                        textView.setText("");
                        textView.append("City: " + weatherData.name + "\n");
                        textView.append("Temperature: " + weatherData.main.temp + "\n");
                        textView.append("Humidity: " + weatherData.main.humidity + "\n");
                    }

                    @Override
                    public void onFailure(Call<WeatherData> call, Throwable t) {
                        textView.append("Error occurred while getting request!");
                        t.printStackTrace();
                    }
                });
    }
}